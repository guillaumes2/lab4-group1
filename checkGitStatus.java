//Lab 4 Group 1
//Due 25 September 2015
import java.util.Scanner;
import java.lang.ProcessBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.*;

public class checkGitStatus{
    public static void main(String[]args){

/*        //create file for output
        try{
            ProcessBuilder getDirectory = new ProcessBuilder("/bin/bash", "pwd > directory.txt", "Argument1");
            Process directory = getDirectory.start();
            directory.waitFor();
        }catch(Exception e1){
            e1.printStackTrace();
        }
        ArrayList<String> directoryArray = new ArrayList<String>();
        try(BufferedReader br2 = new BufferedReader(new FileReader("/home/g/guillaumes/repos/280/lab4-group1/directory.txt"))){
            String repositoryLocation;
            while((repositoryLocation = br2.readLine()) != null){
                directoryArray.add(repositoryLocation);
                System.out.println("Directory Location: " + repositoryLocation);
            }
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("error reached");
        }
        if(directoryArray.size() != 0){
            String repoLocation = directoryArray.get(0);
        }
*/


        File listofGit = new File("/home/g/guillaumes/repos/280/lab4-group1/");
        //initialize scanner
        Scanner scan = new Scanner(System.in);
        //prompt user to enter directory in which to search for git repositories, scan in answer
        System.out.println("Enter the name of the folder in which you would like to search for git repositories: ");
        System.out.println("Use the proper format from the home directory, and if you would like to search everything in the home directory simply enter a \"/\"");
        String folder = scan.nextLine();
        //sets command to search for git repositories and pipe the results into listofGit.txt
        String command1 = "find "+folder+" -type d -name '.git' > listofGit.txt";
        try{
            FileWriter fw = new FileWriter("list.sh");
            PrintWriter pw = new PrintWriter(fw);
            pw.println(command1);
            pw.close();
        }
        catch(IOException e1){
            e1.printStackTrace();
        }

        //run the commands to collect list of git repositories
        //ProcessBuilder pb = new ProcessBuilder("/bin/bash" , command1);
        //pb.directory(listofGit);
        try{
            ProcessBuilder pb = new ProcessBuilder("/bin/bash", "list.sh", "Argument1");
            pb.directory(listofGit);
            Process process = pb.start();
            process.waitFor();
        } catch(Exception e){
            e.printStackTrace();
        }

        //Now take list.sh and read file into an ArrayList using a while loop.
        //Then take the ArrayList and run git status for each one.
        //  do I want to store the results in a different list, then use a parser to add color in the terminal
        //  or do I want to use an if/else statement at the moment to immediately search the result for a word, figure out what color it should be and save it to a specific list
        //    then in that list it will be easier to print by color per list

        //read in list of Git repositories line by line into ArrayList
        ArrayList<String> gitArray = new ArrayList<String>();
        try(BufferedReader br = new BufferedReader(new FileReader("/home/g/guillaumes/repos/280/lab4-group1/listofGit.txt"))){
            String line;
            while((line = br.readLine()) != null){
                gitArray.add(line);
            }
        }catch(Exception e){
            e.printStackTrace();
        }



            String command3 = "git status > results.txt";
        //list 3 sets up command for running git status
        try{
            FileWriter fw3 = new FileWriter("list3.sh");
            PrintWriter pw3 = new PrintWriter(fw3);
            pw3.println(command3);
            pw3.close();
        }
        catch(IOException e1){
            e1.printStackTrace();
        }


        //set commands for performing git status command
        File results = new File("/home/g/guillaumes/repos/280/lab4-group1/");
        int sizeGitList = gitArray.size();
        int a = 0;
        int length;
        while(a<sizeGitList){
            System.out.println("inside while loop");
            String statusInfo = gitArray.get(a);
            a++;
            System.out.println(statusInfo);
            length = statusInfo.length();
            statusInfo = statusInfo.substring(0, length-4);
            System.out.println(statusInfo);
            String command2 = "cd "+statusInfo;
            //list 2 sets up command for changing directory
            try{
                FileWriter fw2 = new FileWriter("list2.sh");
                PrintWriter pw2 = new PrintWriter(fw2);
                pw2.println(command2);
                pw2.close();
            }
            catch(IOException e1){
                e1.printStackTrace();
            }
            //now run process to cd to appropriate git folder, and then run git status
            try{
                ProcessBuilder pb2 = new ProcessBuilder("/bin/bash", "list2.sh", "list3.sh", "Argument1");
                pb2.directory(results);
                Process process2 = pb2.start();
                process2.waitFor();
                System.out.println("inside executing part for git status");

            } catch(Exception e){
                e.printStackTrace();
            }




 try{
            ProcessBuilder pb = new ProcessBuilder("/bin/bash", "list.sh", "Argument1");
            pb.directory(listofGit);
            Process process = pb.start();
            process.waitFor();
        } catch(Exception e){
            e.printStackTrace();
        }






            ArrayList<String> resultArray = new ArrayList<String>();
            try(BufferedReader br2 = new BufferedReader(new FileReader("/home/g/guillaumes/repos/280/lab4-group1/results.txt"))){
                String line2;
                while((line2 = br2.readLine()) != null){
                    resultArray.add(line2);
                    System.out.println("should be printing out information from the file?" +line2);
                }
            }catch(Exception e){
                e.printStackTrace();
                System.out.println("error reached");
            }
            System.out.println(resultArray);
            //issue: never runs git status and puts lines in results.txt, thus the array is empty
            //issue is the .git extension, need to parse so as to remove that before running for git status.





        }



    }
}
