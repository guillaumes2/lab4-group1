###Description of Roles
Team 3
Ayodele Hamilton
Elizabeth Persons 
SJ Guillaumes

###The Requirements Analyst:
The requirements analyst job is to communicate with the customer to understand the type of program that is needed. The customer and the requirements analyst will collaborate to make a program that is obtainable in a short period of time but works well given the short period of time and has few features that work well.

###The Designer:
The designer takes the requirements document and makes a visual diagram that will show how the program will be implemented and how the program is formatted. it shows step by step processes on how the program should operate and the different features that will be implemented in the development of the program. 

###The Implementor:
The implementor uses both the requirements document and the design of the program to help formulate the program of what the customer is asking for. 

 
