# GitBeagle System Design

## Input

Root directory

## Output

List of all repositories under that root and diagnostics for each.

##Class

gitBeagle.java 

##Description

At the moment, our gitBeagle program will use only one class, as that is all that is necessary to implement the basic requirements of our initial release. The gitBeagle class will contain the main() method, which will recursively search through the input root file for any git respositories. It will then go back and check the status of each repository, print its name in the terminal, and list its files along with each file's current state (either updated, modified, committed, or unrecognized).