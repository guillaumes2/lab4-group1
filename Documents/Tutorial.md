# GitBeagle Tutorial

1. Navigate the terminal to the file where gitBeagle.java is saved. Compile the program by typing the command "javac gitBeagle.java", then run it by typing "java gitBeagle".

2. You will see a message asking for the name of the folder in which you would like to search for git repositories. In the terminal, either type the path from your home directory to the file you would like to search, or simply type “/” if you would like to search the entire home directory.  (See Figure 1)

3. In the terminal, you should have a list of all of the repositories in the root file you provided, along with the current status of each file within each repository. 