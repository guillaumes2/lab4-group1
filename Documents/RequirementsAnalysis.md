###Requirements Analysis:
Team 3:
Ayodele Hamilton
Elizabeth Persons 
SJ Guillaumes

###Summary:
The repositories in a user's github, bitbucket or any other website to store source code, can start to get congested and create complications in the repository. The customer wants a program that will help git users control the congestion in the different repositories. The program is a tool for users to unclog their repositories and keep track of what is going on with each file. The files in each repository will do a status check and organized the repositories. The program will be iterate through each repository to find defaults in files. 

###Inputs:
The inputs of the program will be a repository such as bitbucket, github, and others.

###Outputs:
The result of the program will show the different repositories and the status of each file. In the terminal window, the repository are being iterated through to get the files based on the type of file that is found in each repository.

###Behavior:
The program will take in each repository and will conduct a test to see what is happening to each file. The program will read in each repository and will do a "git status" on each repository. The git status is a tool used to get the status of a file. It shows whether or not a file has been committed, committed but not added, or has a merge conflict in the repository. After each repository has been iterated through it give the git status for each file and prints it into the terminal.
